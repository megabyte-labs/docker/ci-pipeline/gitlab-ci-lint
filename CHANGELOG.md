# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## 1.1.0 (2021-06-16)

### [2.3.1](https://gitlab.com/megabyte-labs/dockerfile/ci-pipeline/prettier/compare/v1.0.2...v2.3.1) (2021-06-14)

### [1.0.2](https://gitlab.com/megabyte-labs/dockerfile/ci-pipeline/prettier/compare/v1.0.1...v1.0.2) (2021-06-13)

### [1.0.1](https://gitlab.com/ProfessorManhattan/code/compare/v0.0.25...v1.0.1) (2021-05-15)

### [0.0.25](https://gitlab.com/ProfessorManhattan/code/compare/v0.0.24...v0.0.25) (2021-05-14)

### [0.0.24](https://gitlab.com/ProfessorManhattan/code/compare/v0.0.23...v0.0.24) (2021-05-13)

### 0.0.23 (2021-05-13)
